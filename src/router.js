import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    },
    {
      path: "/scg",
      name: "scg",
      // route level code-splitting
      component: () => import("./views/scg.vue")
    },
    {
      path: "/apisample",
      name: "apisample",
      // route level code-splitting
      component: () => import("./views/apisample.vue")
    },
    {
      path: "/mycv",
      name: "mycv",
      // route level code-splitting
      component: () => import("./views/mycv.vue")
    }
  ]
});
